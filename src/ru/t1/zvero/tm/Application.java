package ru.t1.zvero.tm;

import ru.t1.zvero.tm.constant.TerminalConst;

import static ru.t1.zvero.tm.constant.TerminalConst.*;

public class Application {

    public static void main(final String[] args) {
        processArguments(args);
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            showError();
            return;
        }
        processArguments(args[0]);
    }

    private static void processArguments(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ABOUT:
                showAbout();
            break;
            case VERSION:
                showVersion();
            break;
            case HELP:
                showHelp();
            break;
        }
    }

    private static void showError() {
        System.err.println("[ERROR]");
        System.err.println("this argument not supported...");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Elena Volnenko");
        System.out.println("e-mail: elena@volnenko.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0.");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show about program. \n", ABOUT);
        System.out.printf("%s- Show program version.\n", VERSION);
        System.out.printf("%s - Show list arguments. \n", HELP);
    }

}